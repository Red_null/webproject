@extends('layouts.main')
@section('content')


<div id="main">
    <div class="container col">
        <h3>Admin page</h3>
        <a href="{{route('addActionForm')}}" class="btn btn-primary mx-1">Add action</a>
        <a href="{{route('listAction')}}" class="btn btn-primary mx-1">Update action</a>
        <a href="#" class="btn btn-primary mx-1">#</a>
        <a href="#" class="btn btn-primary mx-1">#</a>
        <a href="#" class="btn btn-primary mx-1">#</a>
        <a href="#" class="btn btn-primary mx-1">#</a>
    </div>
</div>

@endsection
