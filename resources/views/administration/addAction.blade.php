@extends('layouts.main')
@section('content')
    <style>
        .col > input, .col > textarea {
            width: 100%;
        }</style>
    <script>
        function addImg() {
            $("#otherImg").append("<div class='row'><div class='col'><input type='url' placeholder='https://example.com' name='images[]'> </div></div>");
        }

        function removeImg() {
            $("#otherImg .row").last().remove();
        }

        function addLocation() {
            $("#locations").append("<div class='row'><div class='col'><input type='url' placeholder='https://2gis.kz/...' name='locations[]'> </div></div>");
        }

        function removeLocation() {
            $("#locations .row").last().remove();
        }

        function fill() {
            $('.col input').val("111");
            $('.col textarea').val("222");
        }

    </script>
    <body>
    <h3>New action</h3>
    <button onclick="fill()">fill</button>
    <form class="container-fluid" style="width:600px;" method="post" name="actionForm" action="{{route('addAction')}}">
        {{ csrf_field() }}
        <div class="row my-1">
            <div class="col"><label for="head">Heading</label></div>
            <div class="col"><input name="head" type="text" id="head" required></div>

        </div>
        <div class="row my-1">
            <div class="col"><label for="link">Link (lrv.proj.com/offer/{link})</label></div>
            <div class="col"><input name="link" type="text" id="link"></div>

        </div>
        <div class="row my-1">

            <div class="col"><label for="price">Price</label></div>
            <div class="col"><input name="price" type="number" id="price"></div>

        </div>
        <div class="row my-1">
            <div class="col"><label for="descr">Description</label></div>
            <div class="col"><textarea name="descr" cols="10" rows="5" type="text" id="descr" required></textarea></div>

        </div>
        <div class="row my-1">
            <div class="col"><label for="feature">Feature</label></div>
            <div class="col"><textarea name="feature" cols="10" rows="5" type="text" id="feature"></textarea></div>

        </div>
        <div class="row my-1"><p>Category</p></div>

            <div class="row my-1">
                <?php $counter=0 ?>
                    @foreach(( new App\Category)->cursor() as $item)
                        @if($counter%3==0)
                        </div>
                        <div class="row my-1">
                            @endif
                            <div class="container col">
                                <p><input type="checkbox" name="categories[]" value="{{$item->id}}"> {{$item->category_name}}</p>
                            </div>
                            <?php $counter++ ?>
                    @endforeach

            </div>


        <div class="row my-1">

            <div class="col">
                <label for="start">start</label>
                <input name="start" type="date" id="start">
            </div>
            <div class="col">
                <label for="end">end</label>
                <input name="end" type="date" id="end">
            </div>

        </div>


        <div class="row my-1">
            <div class="col"><label for="primaryImg">Primary img</label></div>
            <div class="col"><input name="mainImg" type="text" placeholder="https://example.com" id="primaryImg"
                                    required>
            </div>
        </div>

        <div class="row my-1 align-items-center">
            <div class="col"><span>Secondary img</span></div>
            <div class="col-5 btn-group btn-group-lg" role="group" aria-label="imgFieldControl">
                <a class="btn btn-danger container-fluid" type="button" onclick="removeImg()"><h3>-</h3></a>
                <a class="btn btn-success container-fluid" type="button" onclick="addImg()"><h3>+</h3></a>
            </div>
            <div class="col"></div>
        </div>

        <div class="" id="otherImg">
            <!--secondary images -->
        </div>

        <div class="row my-1 align-items-center">
            <div class="col"><span>Locations</span></div>

            <div class="col-5 btn-group btn-group-lg" role="group" aria-label="locationFieldControl">
                <a class="btn btn-danger container-fluid" type="button" onclick="removeLocation()"><h3>-</h3></a>
                <a class="btn btn-success container-fluid" type="button" onclick="addLocation()"><h3>+</h3></a>
            </div>
            <div class="col"></div>
        </div>

        <div class="" id="locations">
            <!--locations -->
        </div>

        <div class="row my-2">
            <div class="col">
                <button class="btn btn-primary container-fluid" type="submit" id="submit">Add action</button>
            </div>
        </div>

    </form>

@endsection
