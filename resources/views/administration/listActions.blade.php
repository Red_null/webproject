@extends('layouts.main')
@section('content')

    <table class="table ">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">heading</th>
            <th scope="col">price</th>
            <th scope="col">categories</th>
            <th scope="col">change</th>
            <th scope="col">delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach((new App\Action)->cursor() as $item)
            <tr>
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->heading}}</td>
                <td>{{$item->price}}</td>
                <td>
                    @foreach($item->categories()->cursor() as $cat)
                        <span class="badge bg-warning">{{$cat->category_name}}</span>
                    @endforeach
                </td>
                <form id="{{'delete'.$item->id}}" action="{{ route('deleteAction',['id'=>($item->id)]) }}" method="post" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <td><a href="{{route('updateAction',['id'=>($item->id)])}}" class="btn btn-primary">Change</a></td>
                <td><a href="" onclick="event.preventDefault();
                       document.getElementById('{{'delete'.$item->id}}').submit()" class="btn btn-danger">Delete</a></td>


            </tr>

        @endforeach
        </tbody>
    </table>

@endsection