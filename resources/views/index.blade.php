@extends('layouts.main')
@section('content')
<div id="sortBar" class="container my-1">
    <div class="row align-items-center justify-content-center">
        <div class="col-4">
            <label>Сортировать:</label>
        </div>


        <form class="col-5 form-inline" id="sorting" name="sorting" method="get"
              action="{{route('categorySort',compact('category_name'))}}">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sorting" id="popular" value="popular"
                       onclick="this.form.submit();"  checked>
                <label class="form-check-label" for="popular">
                    Популярные
                </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sorting" id="price" value="price"
                       onclick="this.form.submit();" >
                <label class="form-check-label" for="price">
                    Цена
                </label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sorting" id="new" value="new"
                       onclick="this.form.submit();" >
                <label class="form-check-label" for="new">
                    Новые
                </label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sorting" id="name" value="name"
                       onclick="this.form.submit();">
                <label class="form-check-label" for="discount">
                    Название
                </label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="sorting" id="rate" value="rate"
                       onclick="this.form.submit();" >
                <label class="form-check-label" for="rate">
                    Рейтинг
                </label>
            </div>
        </form>
    </div>

    <script>
        $('#{{ $sort }}').prop('checked', true);
    </script>

</div>

<div id="main" class="container">
    <div id="cards" class="container-fluid">
        <div class="row align-items-center justify-content-center">


            <?php $actionCounter = 0 ?>

            @foreach ($actions as $action)
                @if($actionCounter%3==0)
        </div>
        <div class="row align-items-center justify-content-center">
            @endif
            <div class="col-md card bg-light my-2 mx-1">
                {{$action->id}}
                <a href="/offer/{{$action->link}}" class="card-body cardImg my-2"
                   style="background-image: url({{$action->main_img}});"></a>
                <p class="card-text">
                    {{$action->heading}}
                </p>
                <h3 class="mx-auto"><span class="badge badge-warning">{{$action->price}} 〒</span></h3>
                <h3 class="mx-auto"><span class="badge badge-info">с {{date('d/m/y',strtotime($action->date_start))}}</span> <span class="badge badge-info">до {{date('d/m/y',strtotime($action->date_end))}}</span></h3>
            </div>
            <?php $actionCounter++ ?>
            @endforeach

        </div>
        <nav id="paginationNav">
            {{$actions->appends(['sorting' => $sort])->links()}}
            <script>
                $('#paginationNav ul').addClass('justify-content-center pagination-lg');
                $('#paginationNav ul li').addClass('page-item');
                $('#paginationNav ul li span').parent().addClass('page-item disabled');
                $('#paginationNav ul li a').addClass('page-link');
                $('#paginationNav ul li span').addClass('page-link');
            </script>
        </nav>
    </div>



</div>
@endsection