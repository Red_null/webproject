@extends('layouts.main')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="list-group container my-3">
        <h3 class="list-group-item list-group-item-warning">Ваши вопросы:</h3>
        @foreach(Auth::user()->questions()->orderBy('updated_at','desc')->cursor() as $item)
            <div class="list-group-item">
                <p>
                    <span class="badge badge-warning">{{ date('d/m/y',strToTime($item->updated_at)) }}
                        </span>
                    {{$item->question}}
                </p>
                <a href="{{route('showOffer',$item->action()->link)}}"><span class="fa fa-link"></span> {{$item->action()->heading}}</a>
            </div>
        @endforeach
    </div>
@endsection
