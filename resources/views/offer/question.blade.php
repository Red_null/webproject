@foreach($questions as $question)
<hr>
<h4>
    <span class="badge">{{$question->user()->name}}</span>
    {{$question->question}}
    <span class="badge badge-warning"> {{date('d/m/y',strToTime($question->updated_at))}}</span>
</h4>
    @endforeach