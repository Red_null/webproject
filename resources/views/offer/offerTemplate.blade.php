@extends('layouts.main')
@section('content')
    <div id="main" class="container-fluid">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">

                <div class="col card text-center bg-light my-2">

                    <div class="card-header">
                        <div class="row justify-content-between align-items-center" id="dateCap">


                            <p class="col">Можно купить с {{date('d/m/y',strToTime($action->date_start)) }} </p>

                            <p class="col">Можно воспользоваться
                                до {{ date('d/m/y',strtotime($action->date_end)) }} </p>
                        </div>
                    </div>

                    <div class="card-body my-2 container">
                        <h5>{{$action->heading}}</h5>

                        <div class="row justify-content-between align-items-center">
                            <div id="carouselControl" class="col-sm carousel slide" data-ride="carousel">
                                <div class="carousel-inner" id="mainCarousel">

                                    @foreach($images as $key => $image)
                                        @if($key==0)
                                            <div class="carousel-item active">
                                                @else
                                                    <div class="carousel-item">
                                                        @endif

                                                        <img class="d-block w-100" src="{{$image->ref}}">
                                                    </div>

                                                    @endforeach

                                            </div>
                                            <a class="carousel-control-prev" href="#carouselControl" role="button"
                                               data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselControl" role="button"
                                               data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            </a>
                                </div>
                                <div class="col-sm-5 align-self-start" id="cardDescr">
                                    <h3>{{$action->price}} тг.</h3>
                                    <a href="#" class="btn btn-warning">Купить</a>
                                    <p>Купили <b>999</b> человек</p>
                                    <div class="divider"></div>
                                    <p>До завершения акции осталось:</p>

                                    <script>
                                        var countDownDate = new Date('{{$action->date_end}}').getTime();
                                        var x = setInterval(function () {
                                            var now = new Date().getTime();
                                            var distance = countDownDate - now;
                                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                            $("#timer").text(days + "д " + hours + "ч "
                                                + minutes + "м " + seconds + "с");
                                            if (distance < 0) {
                                                clearInterval(x);
                                                $("#timer").text("Акция окончена");
                                            }
                                        }, 1000);
                                    </script>

                                    <span class="fa fa-clock-o fa-2x"></span>
                                    <p id="timer"></p>
                                    <div class="divider"></div>
                                    <div class="col">
                                        <h4>Категории:</h4>
                                        <div class="row">
                                            <?php $tagCounter = 0 ?>
                                            @foreach($action->categories()->cursor() as $tag)
                                                @if($tagCounter%3==0)
                                        </div>
                                        <div class="row">
                                            @endif
                                            <div class="col">
                                                <h5>
                                                    <span class="container badge badge-warning"> {{$tag->category_name}}</span>
                                                </h5>
                                            </div>
                                            <?php $tagCounter++?>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>

                <div class="card text-center">
                    <div class="card-header">
                        <nav>
                            <div class="nav nav-pills nav-justified mx-2 my-4" id="infoNav" role="tablist">

                                <a class="nav-item nav-link active mx-4" id="navInfoTab" data-toggle="tab"
                                   href="#nav-info"
                                   role="tab" aria-controls="nav-info" aria-selected="true">Информация</a>
                                <a class="nav-item nav-link mx-4" id="navReviewTab" data-toggle="tab" href="#nav-res"
                                   role="tab" aria-controls="nav-res" aria-selected="false">Отзывы<span
                                            class="badge badge-light">999</span></a>
                                <a class="nav-item nav-link mx-4" id="navQuestionTab" data-toggle="tab"
                                   href="#nav-question"
                                   role="tab" aria-controls="nav-question" aria-selected="false">Вопросы</a>

                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active container" id="nav-info" role="tabpanel"
                                 aria-labelledby="navInfoTab">
                                <div class="row justify-content-between align-items-center">
                                    <div class="col">
                                        <?=($action->description)?>
                                    </div>
                                    <div class="col">
                                        <script type="text/javascript">
                                            var map;
                                            var point = [ {{$location->first() -> coordinates or ''}} ]; //
                                            DG.then(function () {
                                                map = DG.map('map', {
                                                    center: point,
                                                    zoom: 13
                                                });

                                                DG.marker(point).addTo(map);//.bindPopup('marker txt');
                                            });
                                        </script>
                                        <div class="col" id="map" style="height: 350px"></div>

                                        <?=($action->featureInfo)?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-res" role="tabpanel" aria-labelledby="navReviewTab">
                                Отзывы
                                //todo
                            </div>


                            <div class="tab-pane fade" id="nav-question" role="tabpanel"
                                 aria-labelledby="navQuestionTab">
                                <form id="ask" method="post">
                                    {{ csrf_field() }}
                                    <input type="text" name="data">
                                    <button class="btn btn-warning" type="submit">Задать вопрос</button>
                                </form>

                                <script>
                                    var questionForm = $('#ask');
                                    $(document).ready(function () {
                                        questionForm.submit(function (event) {
                                            event.preventDefault();
                                            axios
                                                .post('{{route('storeQuestion',compact('id','offerName'))}}', questionForm.serialize())
                                                .then(function (response) {
                                                    updateQuestions();
                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                });

                                        });
                                    });

                                    function updateQuestions() {
                                        axios.get(
                                            '{{ '/offer/'.$action->id.'/questions' }}'
                                        ).then(function (q) {
                                            $('#questionBlock').html(q.data);
                                        });
                                    }


                                </script>


                                <div>


                                </div>
                                <div id="questionBlock">
                                    @include('offer.question')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
