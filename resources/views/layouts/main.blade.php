<!DOCTYPE html>
<!-- header -->
<head>
    <meta name="author" content="Luboshnikov_Anton">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<!-- header end-->

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

<!-- nav -->
<div class="row container-fluid align-items-center justify-content-center" id="upperHeader">
    <div id="logo" class="col" style="cursor: pointer;" onclick="window.location='/'"></div>
    <div class="col-4 d-none d-md-block">
        <h5>Главное, чтобы Вы были счастливы!</h5>
    </div>


    <form class="form-inline form-row col-5">
        <input class="form-control col mr-1" type="search" placeholder="Поиск" aria-label="Search">
        <button class="btn btn-outline-success col-3 my-1" type="submit">
            <span class="fa fa-search"></span>
        </button>
    </form>
</div>

<div class="row container-fluid justify-content-end align-items-center" id="lowerHeader">
    <div class="row align-items-center mr-3">
        @if( Auth::guest() )
            <a class="col mr-auto btn btn-warning" type="button" href="{{route('register')}}">Регистрация</a>
            <a class="col btn btn-light" href="{{route('login')}}">Вход</a>
        @else
            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="{{route('home')}}" type="button" class="btn btn-warning">{{Auth::user()->name}}</a>

                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                <div class="dropdown-menu" style="z-index: 1030;" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{route('home')}}">Личный кабинет</a>
                    @if(Auth::id() == 2)
                        <a class="dropdown-item" href="{{route('adminPage')}}">Admin page</a>
                        @endif
                    <a class="dropdown-item" href="#" onclick="event.preventDefault();
                                                     $('#logout-form').submit();">Выход</a>
                </div>


            </div>

        @endif
        <a class="col btn btn-light" href="#">
            <span class="fa fa-shopping-cart"></span>
            <span class="badge badge-light">999</span>
        </a>
    </div>
</div>

<nav class="nav nav-pills  navbar navbar-dark bg-dark nav-fill sticky-top navbar-expand-md justify-content-end"
     id="mainNav">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navToggle"
            aria-controls="navToggle" aria-expanded="true" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse row align-items-center" id="navToggle">
        <div class="navbar-nav col">
            <a class="nav-link nav-item" href="/all">Все</a>
            <a class="nav-link nav-item" href="/category/new">Новые</a>
            <a class="nav-link nav-item" href="/category/hit">Хиты продаж</a>
            <a class="nav-link nav-item" href="/category/entertainment">Развлечения и отдых</a>
            <a class="nav-link nav-item" href="/category/beauty">Красота и здоровье</a>
            <a class="nav-link nav-item" href="/category/sport">Спорт</a>
            <a class="nav-link nav-item" href="/category/product">Товары</a>
            <a class="nav-link nav-item" href="/category/service">Услуги</a>
            <a class="nav-link nav-item" href="/category/food">Еда</a>
            <a class="nav-link nav-item" href="/category/tourism">Туризм и Отели</a>
        </div>
    </div>
</nav>
<!-- nav end-->

<body>

@yield('content')

</body>
<footer class="container-fluid jumbotron">
    <div class="row align-items-center justify-content-center">
        <div class="col-sm">
            <h5>ВСЕ СКИДКИ И АКЦИИ В ОДНОМ МЕСТЕ!</h5>
            <p>Человеку для счастья нужно совсем мало: оставаться здоровым, иметь крепкую семью и хорошую работу,
                хранить гармонию души. Но иногда это не все. Не хватает какого-то приятного дополнения, например,
                получить бешеную скидку на популярную услугу. И этот бонус можем подарить вам именно мы! Благодаря
                купонам, приобретенным в нашем сервисе, вы сможете стать самым счастливым человеком в любой точке
                города Алматы.</p>

            <p>Мы предлагаем скидки в Алматы на самые разнообразные виды деятельности и услуги, которые распределены
                по таким рубрикам:</p>

            <p>полная подборка (все самые актуальные);<br>
                новые (самые свежие предложения);<br>
                санатории (отдых и лечение в наиболее престижных санаториях);<br>
                красота (услуги салонов красоты, а также отдельных мастеров);<br>
                здоровье и спорт (услуги спортзалов, фитнес-клубов и т. д.);<br>
                еда (рестораны, кондитерские, суши, доставка и т. д.);<br>
                развлечения (караоке, сауна и т. д.);<br>
                услуги (авто-услуги, свадебные услуги и т. д.);<br>
                товары (для школы, для дома и т. д.);<br>
                отдых (туризм, гостиницы, отели и т. д.).</p>
        </div>
        <div class="col-sm">
            <h5>ПОКУПАЙТЕ КУПОНЫ И ЭКОНОМЬТЕ НА УСЛУГАХ И ТОВАРАХ!</h5>
            <p>
                В нашем купонном сервисе Шоколайф можно приобрести горящие скидки и купоны в Алматы с самым разным
                акционным диапазоном: от 30% до 90%. На сайте представлено более 530 разных возможностей для
                реализации личных планов. Например, вы можете в очередной раз посетить «Sarafan cafe», но теперь со
                скидочным купоном на 50% оставить там намного меньшую сумму. Такой купон обойдется вам всего в 399
                тенге.</p>

            <p>Срок действия каждой акции в Алматы указан на нашем сайте. Эта информация поможет приобрести купон
                для посещения вами не только интересного предложения, но и оптимального во времени. Чтобы быстрее
                отыскать нужную акцию в нашем агрегаторе скидок, воспользуйтесь удобным поиском сайта онлайн по всем
                имеющимся рубрикам.</p>

            <p>Отныне и навсегда сайт акций и скидок в Алматы должен стать вашим лучшим другом и помощником! С нашей
                помощью вы сэкономите уйму денежных средств, невозвратимое время и дорогое здоровье. Удобное
                оформление сайта, правильно подобранная цветовая гамма, удобство в расположении рубрик не оставят
                равнодушным ни молодежь, ни людей преклонного возраста.</p>

            <p>Скидочные купоны в Алматы, представленные у нас — это гарантия настоящего качества. Мы ручаемся за
                каждого своего компаньона, уверены в безупречности каждого предложения, ведь все проверено лично
                нами! Если вы не успели применить скидку по назначению, или вас не устроило обслуживание — звоните
                по телефону, указанному на сайте, и наша служба заботы о пользователях обязательно даст вам все
                разъяснения. При возникновении вопросов или предложений также можно связаться с нами через форму
                обратной связи.</p>
        </div>
    </div>
    <div class="row divider"></div>
    <div class="row align-items-start justify-content-center">
        <div class="col-sm">
            <h4>Компания</h4>
            <a href="#">О Chocolife.me</a><br>
            <a href="#">Пресса о нас</a><br>
            <a href="#">Контакты</a><br>

        </div>
        <div class="col-sm">
            <h4>Клиентам</h4>
            <a href="#">Обратная связь</a><br>
            <a href="#">Обучающий видеоролик</a><br>
            <a href="#">Вопросы и ответы</a><br>
            <a href="#">Публичная оферта</a><br>
        </div>
        <div class="col-sm">
            <h4>Партнерам</h4>
            <a href="#">Для Вашего бизнеса</a>
        </div>
        <div class="col-sm">
            <h4>Наше приложение</h4>

            <p>Chocolife.me теперь еще удобнее и
                всегда под рукой!</p>
            <a href="#" id="appAndroid"></a><br><br>
            <a href="#" id="appIos"></a>
        </div>

    </div>
</footer>

