<?php
use App\Http\Middleware;
//category and sorting
Route::get('/', 'paginateController@sort')->name('index');
Route::get('/all', 'paginateController@sort');
Route::get('/category/{category_name?}', 'paginateController@sort')->name('categorySort');

//offers
Route::get('/offer/{offerName}', 'offerController@findOffer')->name('showOffer');
Route::get('/offer/{offerId}/questions', 'QuestionController@getQuestions');
Route::post('/offer/{offerName}/question/{id}/post', 'QuestionController@store')->name('storeQuestion');
Route::get('/questions', 'QuestionController@showAll');


//admin
Route::get('/admin', 'AdminController@mainPage' )->name('adminPage')->middleware('auth','admin');
Route::get('/admin/action/add','AdminController@addActionForm')->name('addActionForm')->middleware('auth','admin');
Route::post('/admin/action/store','AdminController@saveAction')->name('addAction')->middleware('auth','admin');
Route::get('/admin/list','AdminController@listAction')->name('listAction')->middleware('auth','admin');
Route::get('/admin/update/{updId}','AdminController@updateForm')->name('updateActionForm')->middleware('auth','admin');
Route::post('/admin/update/{updId}','AdminController@saveAction')->name('updateAction')->middleware('auth','admin');
Route::post('/admin/delete/{id}','AdminController@deleteAction')->name('deleteAction')->middleware('auth','admin');



//crud test
Route::get('/crud', 'crudController@show')->name('showAll');
Route::get('/crud/create', 'crudController@create')->name('createCrud');
Route::post('/crud/store', 'crudController@store')->name('storeCrud');
Route::get('/crud/update/{id}', 'crudController@fillUpdate')->name('fillUpdate');
Route::post('/crud/store-update/{id}', 'crudController@updateCrud')->name('updateCrud');
Route::get('/crud/delete/{id}', 'crudController@deleteCrud')->name('deleteCrud');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
