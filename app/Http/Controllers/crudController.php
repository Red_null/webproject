<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class crudController extends Controller
{
    public function show()
    {
        $table = DB::table('crudTest')->get();
        return view('crud', compact('table'));
    }

    public function create()
    {
        return view('crudCreate');
    }

    public function store(Request $r)
    {

        $newData = $r->data;
        DB::table('crudTest')->insert(
            ['data' => $newData]
        );
        return redirect('/crud');
    }

    public function fillUpdate($id)
    {
        return view('crudUpdate', compact('id'));
    }

    public function updateCrud(Request $r, $id)
    {
        $updateTable = $r->data;
        DB::table('crudTest')
            ->where('id', $id)
            ->update(['data' => $updateTable]);
        return redirect('/crud');
    }

    public function deleteCrud($id)
    {
        DB::table('crudTest')->where('id', '=', $id)->delete();
        return redirect('/crud');
    }
}
