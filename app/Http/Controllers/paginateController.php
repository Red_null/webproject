<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class paginateController extends Controller {
    public function sort(Request $request = null, Category $category_name = null) {
        if ($request == null) {
            $actions = (new \App\Action)->paginate(5);
            $sort = 'popular';
            return view('index', compact('actions', 'sort'));
        }
        $sort = $request->sorting;
        switch ($sort) {
            case "price":
                $orderField = 'price';
                break;
            case "new":
                $orderField = 'date_start';
                break;
            case "name":
                $orderField = 'heading';
                break;
            default:
                $orderField = 'id';
                break;
        }
        if ($category_name->id == null) {
            $actions = (new \App\Action)->orderBy($orderField, 'asc')->paginate(5);
        } else {
            $actions = $category_name->actions()->orderBy($orderField, 'asc')->paginate(5);

        }


        return view('index', compact('actions', 'category_name', 'sort'));
    }

}