<?php

namespace App\Http\Controllers;

use App\Action;
use App\Category;
use App\Img_ref;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller {
    public function mainPage() {
        return view('administration.admin');
    }

    public function addActionForm() {
        return view('administration.addAction');
    }

    public function saveAction(Request $req, $updId = null) {
        if ($updId === null) $action = new Action;
        else
            $action = (new Action)->find($updId);

        $action->description = $req->descr;
        $action->price = $req->price;
        $action->main_img = $req->mainImg;
        $action->heading = $req->head;

        if ($req->link == null) $action->link = $action->max('id') + 1; else $action->link = $req->link;

        $action->feature_info = $req->feature;
        if ($req->start != null) $action->date_start = $req->start;
        if ($req->end != null) $action->date_end = $req->end;

        $action->save();
        if ($req->categories[0] != null) {
            $action->categories()->detach();
            foreach ($req->categories as $cat) {
                $action->categories()->save((new Category)->find($cat));
            }

        }
        if ($req->images[0] != null) {
            $action->img()->delete();
            foreach ($req->images as $img) {
                $ref = new Img_ref(['ref' => $img]);
                $action->img()->save($ref);
            }
        }
        if ($req->locations[0] != null) {
            $action->locations()->delete();
            foreach ($req->locations as $loc) {
              //  if (strpos($loc, '2gis.kz') !== false) {
                    $tmp = substr($loc, strpos($loc, '%2C'));
                    $latLng = substr($tmp, 3, strpos($tmp, '?') - 3);
                    $coord = substr($latLng, strpos($latLng, '%') + 3) . ',' . substr($latLng, 0, strpos($latLng, '%'));
               // } else {
                 //   $coord = $loc;
               // }
                $ref = new Location(['coordinates' => $coord]);
                $action->locations()->save($ref);
            }
        }


        return redirect()->route('adminPage');

    }

    public function listAction() {
        return view('administration.listActions');
    }

    public function updateForm($id) {
        $action = (new Action)->find($id);
        return view('administration.editAction', compact('action'));
    }


    /* public function updateAction(Request $req, $updId) {

         $action = (new Action)->find($updId);
         $action->description = $req->descr;
         $action->price = $req->price;
         $action->main_img = $req->mainImg;
         $action->heading = $req->head;

         if ($req->link == null) $action->link = $action->max('id') + 1;
         else $action->link = $req->link;

         $action->feature_info = $req->feature;
         if ($req->start != null) $action->date_start = $req->start;
         if ($req->end != null) $action->date_end = $req->end;

         $action->save();

         if ($req->categories[0] != null) {
             $action->categories()->detach();
             foreach ($req->categories as $cat) {
                 $action->categories()->save((new Category)->find($cat));
             }
         }

         if ($req->images[0] != null) {
             foreach ($req->images as $img) {
                 $action->img()->delete();
                 $ref = new Img_ref(['ref' => $img]);
                 $action->img()->save($ref);
             }
         }
         if ($req->locations[0] != null) {
             foreach ($req->locations as $loc) {
                 $action->locations()->delete();
                 if (strpos($loc, '2gis.kz') !== false) {
                     $tmp = substr($loc, strpos($loc, '%2C'));
                     $latLng = substr($tmp, 3, strpos($tmp, '?') - 3);
                     $coord = substr($latLng, strpos($latLng, '%') + 3) . ',' . substr($latLng, 0, strpos($latLng, '%'));
                 } else {
                     $coord = $loc;
                 }
                 $ref = new Location(['coordinates' => $coord]);
                 $action->locations()->save($ref);
             }
         }


         return redirect()->route('listAction');

     }*/

    public function deleteAction($id) {
        $action = (new Action)->find($id);
        $action->categories()->detach();
        try {
            $action->delete();
        } catch (\Exception $e) {
        }
        return redirect()->route('listAction');
    }
}
