<?php

namespace App\Http\Controllers;

use App\Action;

class offerController extends Controller
{
    public function findOffer($offerName)
    {
        $action = (new Action)->where('link', $offerName)->first();
        if ($action == null) return redirect('/');

        $id = $action->id;
        $images = $action->img()->get();
        $location = $action->locations()->get();
        $questions = $action->questions()->get();
        return view('offer.offerTemplate', compact('action', 'images', 'location', 'questions', 'offerName', 'id'));
    }


}
