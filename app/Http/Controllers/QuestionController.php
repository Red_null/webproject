<?php

namespace App\Http\Controllers;

use App\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Question;

class QuestionController extends Controller {
    public function showForm($offerName, $id) {
        return view('createQuestion', compact('id', 'offerName'));
    }

    public function store(Request $req, $offerName, $id) {


        $userId = Auth::id();

        if ($userId == null) $userId = 1; //as guest
        $question = new Question;
        $question->question = $req->data;
        $question->action_id = $id;
        $question->user_id = $userId;
        $question->save();
        return;
    }

    public function showAll() {
        $questionTable = (new Question)->get();

        return view('showAllQuestions', compact('questionTable'));
    }

    public function getQuestions($offerId) {
        $questions = (new Action)->find($offerId)->questions()->get();
        return view('offer.question')->with('questions', $questions)->render();
    }
}


