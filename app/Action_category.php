<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action_category extends Model
{
    public $timestamps = false;
    public $table='action_category';

}
