<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Img_ref extends Model
{
    public $timestamps = true;
    public $table='img_ref';
    protected $fillable = ['ref'];
    public function actions(){
        return $this->belongsTo('App\Action');
    }
}
