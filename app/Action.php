<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    public $timestamps = true;
    public function img()
    {
        return $this->hasMany('App\Img_ref');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function locations(){
        return $this->hasMany('App\Location');
    }
    public function questions(){
        return $this->hasMany('App\Question');
    }

}
