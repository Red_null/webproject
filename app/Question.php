<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = true;
    public function user(){
        return $this->belongsTo(User::class)->first();
    }
    public function action(){
        return $this->belongsTo(Action::class)->first();
    }
}
