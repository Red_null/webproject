<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = true;
    public function actions(){
        return $this->belongsToMany(Action::class);
    }
    public function getRouteKeyName(){
        return 'category_name';
    }
}
