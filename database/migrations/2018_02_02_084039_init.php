<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('heading');
            $table->string('main_img');
            $table->string('link');
            $table->text('description');
            $table->text('feature_info');
            $table->integer('price');
            $table->date('date_start')->nullable();
            $table->timestamps();
        });
Schema::table('actions', function (Blueprint $table){
    $table->date('date_end')->nullable();
});

        Schema::create('img_ref', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_id')->unsigned();
            $table->string('ref');
            $table->timestamps();
            $table->foreign('action_id')->references('id')->on('actions');

        });

        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_id')->unsigned();
            $table->string('coordinates');
            $table->timestamps();
            $table->foreign('action_id')->references('id')->on('actions');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('img_ref');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('actions');
    }
}
