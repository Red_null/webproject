<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Category extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name');
            $table->timestamps();
        });

        DB::table('categories')->insert([
            ['category_name'=> 'new'],
            ['category_name'=> 'hit'],
            ['category_name'=> 'entertainment'],
            ['category_name'=> 'beauty'],
            ['category_name'=> 'sport'],
            ['category_name'=> 'product'],
            ['category_name'=> 'service'],
            ['category_name'=> 'food'],
            ['category_name'=> 'tourism']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
